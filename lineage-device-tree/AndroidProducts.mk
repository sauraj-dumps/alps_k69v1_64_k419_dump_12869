#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_k69v1_64_k419.mk

COMMON_LUNCH_CHOICES := \
    lineage_k69v1_64_k419-user \
    lineage_k69v1_64_k419-userdebug \
    lineage_k69v1_64_k419-eng
