#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from k69v1_64_k419 device
$(call inherit-product, device/alps/k69v1_64_k419/device.mk)

PRODUCT_DEVICE := k69v1_64_k419
PRODUCT_NAME := lineage_k69v1_64_k419
PRODUCT_MODEL := k69v1_64_k419
PRODUCT_MANUFACTURER := alps

PRODUCT_GMS_CLIENTID_BASE := android-alps

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_mssi_64_ww-user 13 TP1A.220624.014 mp1rck6985v164P4 dev-keys"

BUILD_FINGERPRINT := /vnd_k69v1_64_k419/k69v1_64_k419:13/TP1A.220624.014/:user/release-keys
